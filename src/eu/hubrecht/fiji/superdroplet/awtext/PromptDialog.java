/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.awtext;

import com.wordpress.tips4java.layout.RelativeLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import javax.swing.Box;
import javax.swing.BoxLayout;

/**
 *
 * @author d.fabreges
 */
public class PromptDialog extends Dialog {
    
    private TextField field;
    private Button    ok;
    private Button    cancel;
    
    private Predicate<String> predicate = null;
    
    private String _string = null;
    private boolean _cancelled = true;
    private boolean _hasBeenVisible = false;
    
    private final String def;
    
    public PromptDialog(Frame frame, String def) {
        super(frame);
        
        this.def = def;
        
        initComponents();
        initEvents();
        
        checkValidity();
    }
    
    private void initComponents() {
        field  = new TextField(this.def);
        ok     = new Button("Ok");
        cancel = new Button("Cancel");
        
        Container input = new Container();
        input.setLayout(new RelativeLayout(RelativeLayout.X_AXIS, 10));
        input.add(field, 1f);
        
        Container buttons = new Container();
        buttons.setLayout(new RelativeLayout(RelativeLayout.X_AXIS, 10));
        
        if (ij.IJ.isMacOSX()) {
            buttons.add(cancel, 1f);
            buttons.add(ok, 1f);
        } else {
            buttons.add(ok, 1f);
            buttons.add(cancel, 1f);
        }
        
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(Box.createVerticalStrut(5));
        this.add(input);
        this.add(Box.createVerticalStrut(5));
        this.add(buttons);
        this.add(Box.createVerticalStrut(5));
        
        this.pack();
    }
    
    private void initEvents() {
        ok.addActionListener(e -> {
            this._string    = field.getText();
            this._cancelled = false;
            this.dispose();
        });
        
        cancel.addActionListener(e -> {
            this.dispose();
        });
        
        field.addTextListener(e -> {
            checkValidity();
        });
        
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                windowEvent.getWindow().dispose();
            }
        });

    }

    private void checkValidity() {
        String s = field.getText();
        
        if (!"".equals(s) && (predicate == null || predicate.test(s))) {
            field.setBackground(Color.WHITE);
            ok.setEnabled(true);
            return;
        }

        field.setBackground(new Color(0xFFA19C));
        ok.setEnabled(false);
    }
    
    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        
        if (b) {
            this._hasBeenVisible = true;
        }
    }
    
    public void addStringValidation(Predicate<String> accept) {
        if (predicate == null) {
            predicate = accept;
        } else {
            predicate = predicate.and(accept);
        }
    }
    
    public boolean wasCancelled() {
        if (this._hasBeenVisible) {
            return this._cancelled;
        }
        
        return false;
    }
    
    public String getString() {
        return this._string;
    }
    
}
