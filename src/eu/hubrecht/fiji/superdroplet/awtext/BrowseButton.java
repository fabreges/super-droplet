/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.awtext;

import java.awt.Button;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;

/**
 *
 * @author d.fabreges
 */
public class BrowseButton extends Button implements ActionListener
{

    private final List<FileSelectionListener> listeners = new ArrayList<>();
    private final String defaultPath;
    
    private String path = null;
    
    public BrowseButton(String defaultPath) throws HeadlessException {
        super();
        this.defaultPath = defaultPath;
        initEvent();
    }
    
    public BrowseButton(String defaultPath, String label) throws HeadlessException {
        super(label);
        this.defaultPath = defaultPath;
        initEvent();
    }
    
    private void initEvent() {
        this.addActionListener(this);
    }

    public void addFileSelectionListener(FileSelectionListener fsl) {
        listeners.add(fsl);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        JFileChooser jfc;
        if (this.path != null)
            jfc = new JFileChooser(this.path);
        else
            jfc = new JFileChooser(this.defaultPath);
        
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int ret = jfc.showOpenDialog(null);
        
        if (ret == JFileChooser.APPROVE_OPTION) {
            this.path = jfc.getSelectedFile().getAbsolutePath();
            listeners.forEach(l -> l.fileSelected(new FileSelectionEvent(this, this.path)));
        }
    }
}
