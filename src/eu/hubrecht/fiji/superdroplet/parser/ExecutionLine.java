/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.parser;

import eu.hubrecht.fiji.superdroplet.Future;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author d.fabreges
 */
public class ExecutionLine {

    public static final int TYPE_OPTIONS    = 1;
    public static final int TYPE_PARAMETERS = 2;
    public static final int TYPE_COMMENTS   = 3;
    
    private final int      type;
    private final String   name;
    private final String[] parameters;
    
    private Object[] values = null;
    
    /**
     * Process a line of the macro and try to interpret the content in the
     * context of the Super Droplet. Typically if it starts with `//!` or `//?`.
     * 
     * @param line
     * @throws ExecutionLineException 
     */
    public ExecutionLine(String line) throws ExecutionLineException {
        if (line.startsWith("//!")) {
            this.type = TYPE_OPTIONS;
        } else if (line.startsWith("//?")) {
            this.type = TYPE_PARAMETERS;
        } else if (line.startsWith("//")) {
            this.type = TYPE_COMMENTS;
            this.name = null;
            this.parameters = null;
            return;
        } else {
            throw new ExecutionLineException("error: unknown execution type: " + line);
        }
        
        line = Future.String.strip(line.substring(3));
        String[] elements = line.split(":");
        this.name = Future.String.strip(elements[0]).toLowerCase();
        
        String rest = "";
        for (int i = 1; i<elements.length; i++) {
            if (i > 1) {
                rest += ":";
            }
            
            rest += elements[i];
        }
        
        this.parameters = splitWithQuotes(rest);
    }
    
    private String[] splitWithQuotes(String str) throws ExecutionLineException
    {
        List<String>  ret = new ArrayList<>();
        StringBuilder sb  = new StringBuilder();
        boolean escaped   = false;
        for (int i = 0; i<str.length(); i++) {
            char c = str.charAt(i);
            
            if (sb.length() == 0 && (c == '\'' || c == '"')) {
                int j = i+1;
                
                try {
                    while (str.charAt(j) != c) {
                        j++;
                    }
                } catch(IndexOutOfBoundsException ex) {
                    throw new ExecutionLineException("error: quote error: " + str);
                }
                
                if (j+1 == str.length() || str.charAt(j+1) == ' ') {
                    ret.add(str.substring(i+1, j));
                    i = j;
                } else {
                    throw new ExecutionLineException("error: syntax error: quote char before non-splitting char: " + str);
                }
            } else if (sb.length() > 0 && (c == '\'' || c == '"') && !escaped) {
                throw new ExecutionLineException("error: syntax error: quote char after non-splitting char: " + str);
            } else {
                if (escaped) {
                    sb.append(c);
                    escaped = false;
                } else {
                    if (c == '\\') {
                        escaped = true;
                    } else if (c == ' ' && sb.length() > 0) {
                        ret.add(sb.toString());
                        sb = new StringBuilder();
                    } else if (c != ' ') {
                        sb.append(c);
                    }
                }
            }
        }
        
        if (sb.length() > 0) {
            ret.add(sb.toString());
        }
        
        return ret.toArray(new String[]{});
    }
    
    protected void parse() throws ExecutionLineException {
        switch (this.type) {
            case TYPE_OPTIONS:
                parseOptions();
                break;
            case TYPE_PARAMETERS:
                parseParameters();
                break;
            case TYPE_COMMENTS:
                break;
            default:
                throw new ExecutionLineException("error: unexpected or unknown execution type: " + this.type);
        }
    }
    
    private void parseOptions() throws ExecutionLineException {
        if (null == name) {
            throw new ExecutionLineException("error: unexpected option: " + name);
            
        } else switch (name) {
            case "group":
            {
                if (parameters.length != 1) {
                    throw new ExecutionLineException("error: bad definition: option 'group' requires exactly one parameter"); }
                String pvalue = parameters[0].toLowerCase();
                if ("none".equals(pvalue) || "folder".equals(pvalue) || "all".equals(pvalue)) {
                    this.values = new Object[]{pvalue};
                } else {
                    throw new ExecutionLineException("error: bad definition: option 'group' must be one of 'none', 'folder' or 'all', not '" + pvalue + "'"); }
                break;
            }
            
            case "recursive":
            {
                if (parameters.length != 1) {
                    throw new ExecutionLineException("error: bad definition: option 'recursive' requires exactly one parameter"); }
                String pvalue = parameters[0].toLowerCase();
                if ("true".equals(pvalue) || "false".equals(pvalue)) {
                    this.values = new Object[]{("true".equals(pvalue))};
                } else {
                    throw new ExecutionLineException("error: bad definition: option 'recursive' must be either 'true' or 'false', not '" + pvalue + "'"); }
                break;
            }
            
            case "type":
            {
                if (parameters.length != 1) {
                    throw new ExecutionLineException("error: bad definition: option 'type' requires exactly one parameter"); }
                String pvalue = parameters[0].toLowerCase();
                if ("file".equals(pvalue) || "folder".equals(pvalue) || "both".equals(pvalue)) {
                    this.values = new Object[]{pvalue};
                } else {
                    throw new ExecutionLineException("error: bad definition: option 'type' must be one of 'file', 'folder' or 'both', not '" + pvalue + "'"); }
                break;
            }
            
            default:
                throw new ExecutionLineException("error: unknown option: " + name);
        }
    }

    private void parseParameters() throws ExecutionLineException {
        if (null == name) {
            throw new ExecutionLineException("error: unexpected parameter: " + name);
        
        
        } else switch (name) {
            case "checkbox":
            {
                if (parameters.length != 2) {
                    throw new ExecutionLineException("error: bad definition: parameter 'checkbox' requires exactly two parameters: checkbox 'label' default={true, false}"); }
                
                this.values = new Object[]{parameters[0], "true".equals(parameters[1])};
                break;
            }
            
            case "choice":
            {
                if (parameters.length < 2) {
                    throw new ExecutionLineException("error: bad definition: parameter 'choice' requires at least 2 parameters: choice 'label' 'option1' 'option2' ..."); }
                
                this.values = parameters;
                break;
            }
            
            case "file":
            {
                if (parameters.length < 1 || parameters.length > 2) {
                    throw new ExecutionLineException("error: bad definition: parameter 'file' requires 1 or 2 parameters: file 'label' ['default path']"); }
                
                this.values = parameters;
                break;
            }
            
            case "message":
            {
                if (parameters.length != 1) {
                    throw new ExecutionLineException("error: bad definition: parameter 'message' requires exactly one parameter: message 'label'"); }
                
                this.values = parameters;
                break;
            }
            
            case "number":
            {
                try {
                    if (parameters.length == 2) {
                        this.values = new Object[]{parameters[0], Double.valueOf(parameters[1])};
                    } else if (parameters.length == 5) {
                        this.values = new Object[]{parameters[0], Double.valueOf(parameters[1]), Integer.valueOf(parameters[2]), Integer.valueOf(parameters[3]), parameters[4]};
                    } else {
                        throw new ExecutionLineException("error: bad definition: parameter 'number' requires exactly 2 or 5 parameters: number 'label' default [decimal columns units]"); }
                } catch (NumberFormatException ex) {
                    throw new ExecutionLineException("error: bad parameter value: parameter 'number' expects exactly 2 or 5 parameters of types: string number [integer integer string]");
                }
                
                break;
            }
            
            case "radio":
            {
                if (parameters.length < 4) {
                    throw new ExecutionLineException("error: bad definition: parameter 'radio' requires at least 4 parameters: radio 'label' rows columns 'option1' 'option2' ..."); }
                
                List<Object> v = new ArrayList<>();
                List<String> t = new ArrayList<>();
                
                try {
                    v.add(parameters[0]);                   t.add("string");
                    v.add(Integer.valueOf(parameters[1]));  t.add("integer");
                    v.add(Integer.valueOf(parameters[2]));  t.add("integer");
                } catch (NumberFormatException ex) {
                    throw new ExecutionLineException("error: bad parameter value: parameter 'radio' expects at least 4 parameters of types: string integer integer string [string...]");
                }
                
                for (int i = 3; i<parameters.length; i++) {
                    v.add(parameters[i]);
                    t.add("string");
                }
                
                this.values = v.toArray(new Object[]{});
                break;
            }
            
            case "slider":
            {
                if (parameters.length != 4) {
                    throw new ExecutionLineException("error: bad definition: parameter 'slider' requires exactly 4 parameters: slider 'label' min max default"); }
                
                try {
                    this.values = new Object[]{parameters[0], Double.valueOf(parameters[1]), Double.valueOf(parameters[2]), Double.valueOf(parameters[3])};
                } catch (NumberFormatException ex) {
                    throw new ExecutionLineException("error: bad parameter value: parameter 'slider' expects exactly 4 parameters of types: string number number number");
                }
                break;
            }
            
            case "string":
            {
                if (parameters.length != 2) {
                    throw new ExecutionLineException("error: bad definition: parameter 'string' requires exactly 2 parameters: string 'label' 'default'"); }
                    
                this.values = parameters;
                break;
            }
            
            
            default:
                throw new ExecutionLineException("error: unknown option: " + name);
        }
    }
    
    /**
     * Retrieves the type of the line, either an option (`//!`) or a parameter (`//?`).
     * @return Either `ExecutionLine.TYPE_OPTIONS` or `ExecutionLine.TYPE_PARAMETERS`.
     */
    public int getType() {
        return type;
    }
    
    /**
     * Gets the name of the option/parameter.
     * @return The name of the option/parameter.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Gets the number of values available for this option/parameter.
     * @return The number of values available for this option/parameter.
     */
    public int getNumValues() {
        return values.length;
    }
    
    /**
     * Gets the i-th value parsed as an integer. No check is performed.
     * @param i The value to retrieve. 
     * @return The i-th value, parsed as an integer.
     */
    public int getIntValue(int i) {
        return (int) values[i];
    }
    
    /**
     * Gets the i-th value parsed as a String. No check is performed.
     * @param i The value to retrieve. 
     * @return The i-th value, parsed as a String.
     */
    public String getStringValue(int i) {
        return (String) values[i];
    }
    
    /**
     * Gets the i-th value parsed as a boolean. No check is performed.
     * @param i The value to retrieve. 
     * @return The i-th value, parsed as a boolean.
     */
    public boolean getBoolValue(int i) {
        return (boolean) values[i];
    }
    
    /**
     * Gets the i-th value parsed as a double. No check is performed.
     * @param i The value to retrieve. 
     * @return The i-th value, parsed as a double.
     */
    public double getDblValue(int i) {
        return (double) values[i];
    }
    
    /**
     * Gets the i-th value, unparsed. No check is performed.
     * @param i The value to retrieve. 
     * @return The i-th value.
     */
    public Object getValue(int i) {
        return values[i];
    }
    
    protected Object[] getAllValues() {
        return values;
    }

}
