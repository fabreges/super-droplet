/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.parser;

import eu.hubrecht.fiji.superdroplet.Future;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author d.fabreges
 */
public class ExecutionBox {

    private List<ExecutionLine> executionLines = new ArrayList<>();
    private final File macro;
    
    public ExecutionBox(File macro) {
        this.macro = macro;
        
        try(BufferedReader br = new BufferedReader(new FileReader(macro))) {
            
            String line;
            while ((line = br.readLine()) != null) {
                line = Future.String.strip(line);
                if (line.length() == 0) {
                    continue;
                }
                
                if (!line.startsWith("//")) {
                    break;
                }
                
                ExecutionLine eline;
                try {
                    eline = new ExecutionLine(line);
                    eline.parse();
                    
                    executionLines.add(eline);
                    
                } catch (ExecutionLineException elex) {
                    ij.IJ.log(elex.getMessage());
                }
            }
            
        } catch (IOException ex) {
            ij.IJ.error("The macro file cannot does not exist or is not readable:\n" + macro.getAbsolutePath());
        }
    }
    
    /**
     * Gets all the values and all the occurences of a specific option from this
     * macro. If the option has been specified multiple times in the macro's header,
     * the list will be greater than one. If the option has not been specified, the
     * list will be empty.
     * @param name
     * @return 
     */
    public List<Object[]> getOptionValues(String name) {
        List<Object[]> ret = new ArrayList<>();
        
        for (ExecutionLine eline : executionLines) {
            if (eline.getType() == ExecutionLine.TYPE_OPTIONS) {
                if (eline.getName().equals(name)) {
                    ret.add(eline.getAllValues());
                }
            }
        }
        
        return ret;
    }

    /**
     * Gets all the execution lines (starting with `//!` or `//?`) parsed by
     * the execution box.
     * @return 
     */
    public List<ExecutionLine> getExecutionLines() {
        return executionLines;
    }
    
    /**
     * Gets the file pointing to the macro.
     * @return 
     */
    public File getMacro() {
        return macro;
    }
}
