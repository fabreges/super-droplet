/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.parser;

/**
 *
 * @author d.fabreges
 */
public class ExecutionLineException extends Exception {

    public ExecutionLineException(String message) {
        super(message);
    }
    
}
