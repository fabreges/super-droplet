/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.awtext;

/**
 *
 * @author d.fabreges
 */
public interface FileSelectionListener {
    
    public void fileSelected(FileSelectionEvent fse);
    
}
