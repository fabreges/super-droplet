/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.awtext;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

/**
 *
 * @author d.fabreges
 */
public class NumericField extends TextField implements TextListener, FocusListener {

    private final Color defaultBackground;
    private final int decimal;
    public NumericField(double defaultValue, int decimal) throws HeadlessException {
        super("" + defaultValue);
        
        this.decimal      = decimal;
        defaultBackground = Color.WHITE;
        initEvent();
    }
    
    private void initEvent() {
        this.addTextListener(this);
        this.addFocusListener(this);
    }

    @Override
    public void textValueChanged(TextEvent e) {
        TextField textfield = (TextField) e.getSource();
        String text = textfield.getText();
        
        try {
            double number = Double.parseDouble(text);
            textfield.setBackground(defaultBackground);
        } catch(NumberFormatException ex) {
            textfield.setBackground(new Color(0xFFA19C));
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        TextField textfield = (TextField) e.getSource();
        String text = textfield.getText();
        
        try {
            double number  = Double.parseDouble(text);
            double rounder = Math.pow(10, decimal);
            number = Math.round(number * rounder) / rounder;
            textfield.setText("" + number);
        } catch(NumberFormatException ex) {
            // Do nothing
        }
    }
    
    @Override public void focusGained(FocusEvent e) { }
    
}
