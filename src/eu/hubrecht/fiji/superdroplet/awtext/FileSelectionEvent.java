/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet.awtext;

import java.awt.Component;

/**
 *
 * @author d.fabreges
 */
public class FileSelectionEvent {
    
    private final Component source;
    private final String filepath;
    
    public FileSelectionEvent(Component source, String filepath) {
        this.source   = source;
        this.filepath = filepath;
    }

    public Component getSource() {
        return source;
    }

    public String getFilepath() {
        return filepath;
    }
}