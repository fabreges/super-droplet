/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.prefs.BackingStoreException;

/**
 *
 * @author d.fabreges
 */
public class InMemoryPreferences {

    private final Map<String, String> data = new HashMap<>();
    private final Map<String, InMemoryPreferences> children = new HashMap<>();
    private final InMemoryPreferences parent;
    private final String name;
    
    public InMemoryPreferences() {
        parent = null;
        name   = "root";
    }
    
    private InMemoryPreferences(InMemoryPreferences parent, String name) {
        this.parent = parent;
        this.name   = name;
    }
    
    public void put(String key, String value) {
        data.put(key, value);
    }
    
    public void put(String key, int value) {
        data.put(key, Integer.toString(value));
    }
    
    public void put(String key, long value) {
        data.put(key, Long.toString(value));
    }

    public void put(String key, float value) {
        data.put(key, Float.toString(value));
    }
    
    public void put(String key, double value) {
        data.put(key, Double.toString(value));
    }
    
    public void put(String key, boolean value) {
        data.put(key, (value ? "true" : "false"));
    }
    
    public void put(String key, byte[] value) {
        data.put(key, Base64.getEncoder().encodeToString(value));
    }
    
    public void remove(String key) {
        data.remove(key);
    }

    public void clear() throws BackingStoreException {
        data.clear();
    }

    public String get(String key, String def) {
        return data.getOrDefault(key, def);
    }

    public int getInt(String key, int def) {
        try {
            return Integer.parseInt(data.get(key));
        } catch (NullPointerException | NumberFormatException ex) {
            return def;
        }
    }

    public long getLong(String key, long def) {
        try {
            return Long.parseLong(data.get(key));
        } catch (NullPointerException | NumberFormatException ex) {
            return def;
        }
    }
    
    public float getFloat(String key, float def) {
        try {
            return Float.parseFloat(data.get(key));
        } catch (NullPointerException | NumberFormatException ex) {
            return def;
        }
    }
    
    public double getDouble(String key, double def) {
        try {
            return Double.parseDouble(data.get(key));
        } catch (NullPointerException | NumberFormatException ex) {
            return def;
        }
    }
    
    public boolean getBoolean(String key, boolean def) {
        try {
            return Boolean.parseBoolean(data.get(key));
        } catch (NullPointerException | NumberFormatException ex) {
            return def;
        }
    }

    public byte[] getByteArray(String key, byte[] def) {
        try {
            return Base64.getDecoder().decode(data.get(key));
        } catch (Exception ex) {
            return def;
        }
    }

    public String[] keys() {
        Set<String> k = data.keySet();
        String[] ret  = new String[k.size()];
        
        return k.toArray(ret);
    }

    public String[] childrenNames() {
        Set<String> k = children.keySet();
        String[] ret  = new String[k.size()];
        
        return k.toArray(ret);
    }

    public InMemoryPreferences parent() {
        return this.parent;
    }

    public InMemoryPreferences child(String name) {
        if (!childExists(name))
            children.put(name, new InMemoryPreferences(this, name));
        
        return children.get(name);
    }

    public boolean childExists(String name) {
        return children.containsKey(name);
    }

    public void removeChild(String name) {
        children.remove(name);
    }

    public String name() {
        return this.name;
    }
    
    public String path() {
        if (this.parent == null) {
            return this.name;
        }
        
        return this.parent.path() + "/" + this.name;
    }
    
}
