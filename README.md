# Super Droplet

Heavily inspired by the plugin [Droplet](https://imagejdocu.list.lu/plugin/utilities/droplet/start)
by Jerome Mutterer and Wayne Rasband, **Super Droplet** is a configurable alternative.

A plugin frame is used to accept Drag and Drop of files, and execute a Macro on the list of files.

<div align="center">

![Screen capture of the droplet plugin, showing a macro and its option ready to receive dropped package](resources/screencapture.png){width=50%}
</div>

## Author

Dimitri Fabrèges (d.fabreges (at) hubrecht.eu)

## Features

- Select a Macro and drop a file on the frame to execute the macro for the droped file
- Macro can be edited by clicking on the `...` button
- Get the dropped files with the command `files = getArgument();`
- Multiple files can be sent to the macro individually, grouped by folders are all together
- Multiple files can be retrieved using `files = split(getArgument(), "\n");`
- Macros can be configured to display settings
- Retrieve the settings using `Dialog.get*` commands

See **How to write a macro** below for details.

## Installation

Download `Super Droplet.jar` to the plugins folder and restart the application or update the menus.

**Super Droplet** uses the same folder as **Droplet** in `.../plugins/Droplet Actions` and is
fully compatible with the macro written for **Droplet**

## Macro example

```
// This macro is used to project Z stack along the X, Y or Z axis.
//! group: folder
//! type: file
//! recursive: true
//? checkbox: 'Set batch mode' true
//? choice: 'Projection axis: ' 'X' 'Y' 'Z'
//? choice: 'Projection type: ' 'Average Intensity' 'Max Intensity' 'Min Intensity' 'Sum Slices' 'Standard Deviation' 'Median'

files = split(getArgument(), "\n");
Array.sort(files);

if (Dialog.getCheckbox()) {
    setBatchMode(true);
}

axis = Dialog.getChoice();
type = Dialog.getChoice();

finalName = "final";
firstrun  = true;
for (f = 0; f<lengthOf(files); f++) {

    filename = File.getName(files[f]);
    if (startsWith(filename, ".")) {
        continue;
    }

    open(files[f]);

    refimg = getImageID();
    if (axis == "X") {
        run("Reslice [/]...", "output=1.000 start=Left");
        selectImage(refimg); close();
        refimg = getImageID();
    } else if (axis == "Y") {
        run("Reslice [/]...", "output=1.000 start=Top");
        selectImage(refimg); close();
        refimg = getImageID();
    }

    run("Z Project...", "projection=[" + type + "]");
    selectImage(refimg); close();

    if (firstrun) {
        finalName = getTitle();
    } else {
        run("Concatenate...", "open title=[" + finalName + "] image1=[" + finalName + "] image2=[" + getTitle() + "]");
    }

    firstrun = false;
}

setBatchMode("exit & display");
```

## How to write a macro

Any macro can be run using **Super Droplet**. However, this plugin also offer advanced options
to make the experience easier.

There are two type of settings: fixed and variable. They must be defined at the beggining of the
macro.

### Fixed settings

Fixed settings are chosen by the person who wrote the macro. Changing the settings will most likely
mean rewritting a significant part of the macro's logic.

To declare a fixed settings, start the line with `//! ` followed by the name of the setting and its
value. There are three fixed settings available:

| name      | possible values      | type    | default value | example                |
|:---------:|:--------------------:|:-------:|:-------------:|:----------------------:|
| recursive | {true, false}        | boolean | false         | `//! recursive: false` |
| group     | {none, folder, all}  | string  | none          | `//! group: none`      |
| type      | {file, folder, both} | string  | both          | `//! type: both`       |

#### recursive

If a folder is dropped on the frame, recursivity will search for sub-files and sub-folders to add
to the list. By default, recursivity is off.

#### group

If multiple files are dropped on the frame (which includes all the sub-files and sub-folders if
recursivity is on), they can be sent individually to the macro (`none`), grouped by same folder
(`folder`), or sent in one bulk to the macro (`all`). The list of files is available using the
`getArgument()` method. When multiple files are sent, filepaths are `\n`-separated. Default value
is `none` (meaning each file is sent to the macro individually).

#### type

Only send the files (`file`) of the folders (`folder`), or both (`both`), to the macro. Default
value is `both`, meaning that all files and folders will be sent to the macro.

### Variable settings

Variable settings will be displayed as a regular form in the drop area. Settings can be adjusted
before dropping files.

To declare a variable settings, start the line with `//? ` followed by the type of settings and its
parameters. The variable settings closely mimic the methods available in `Dialog.add*`.

Note that to use spaces, the parameter's value must be quoted (either with single or double quotes).
Alternatively, a backslash can be used to escape spaces directly.

| name     | parameter  | possible values      | type    | default value | role                                    |
|:--------:|:----------:|:--------------------:|:-------:|:-------------:|:----------------------------------------|
| checkbox | label      | *any*                | string  | *none*        | Label of the checkbox                   |
|          | default    | {true, false}        | boolean | false         | Default state of the checkbox           |
| choice   | label      | *any*                | string  | *none*        | Label of the selection box              |
|          | options... | *any*                | string  | *none*        | Available options of the box            |
| file     | label      | *any*                | string  | *none*        | Label of the file path chooser          |
|          | default    | *any*                | string  | os-specific   | Default directory                       |
| message  | text       | *any*                | string  | *none*        | Text to display                         |
| number   | label      | *any*                | string  | *none*        | Label of the numeric field              |
|          | default    | *any*                | double  | *none*        | Default value                           |
|          | decimal    | ≥0                   | integer | 3             | Number of decimals                      |
|          | columns    | ≥0                   | integer | 0 (as needed) | Character-width of the field            |
|          | units      | *any*                | string  | null value    | Text to display as unit after the field |
| radio    | label      | *any*                | string  | *none*        | Label of the radio grid                 |
|          | rows       | ≥1                   | integer | *none*        | Number of rows in the grid              |
|          | columns    | ≥1                   | integer | *none*        | Number of columns in the grid           |
|          | options... | *any*                | string  | *none*        | Options of the radio button in the grid |
| slider   | label      | *any*                | string  | *none*        | Label of the slider                     |
|          | min        | *any*                | double  | *none*        | Minimum value of the slider             |
|          | max        | *any*                | double  | *none*        | Maximum value of the slider             |
|          | default    | *any*                | double  | *none*        | Default value of the slider             |
| string   | label      | *any*                | string  | *none*        | Label of the text field                 |
|          | default    | *any*                | string  | *none*        | Default value of the text field         |


#### checkbox

Add a checkbox to the available options for the users. Use `Dialog.getCheckbox()` to get the chosen
value.

```
//? checbox: "Set batch mode" false

if (Dialog.getCheckbox()) {
    setBatchMode(true);
}
```

#### choice

Add a selection box that allows the user to choose between a limited number of text options. Use
`Dialog.getChoice()` to get the selected option.

```
//! type: file
//? choice: "Start from: " Left Top Right Bottom

open(getArgument());
run("Reslice [/]...", "output=1.000 start=" + Dialog.getChoice());
```

#### file

Add *Browse...* button to select a folder or a file. Use `Dialog.getString()` to get the selected
filepath.

```
//! type: file
//? file: "Mask: "

open(Dialog.getString()); rename("mask");
open(getArgument());      rename("img");

imageCalculator("Multiply create stack", "img", "mask");
```

#### message

Add a piece of non-interactive text.

```
//? message: "Author: Dimitri Fabrèges"
```

#### number

Add a numeric field that allows the user to input a number. There is (almost) no verification of the
inputs and the passed value is not guaranteed. Use `Dialog.getNumber()` to retrieve the value.

```
//! type: file
//? number: "Sigma X: " 1
//? number: "Sigma Y: " 1
//? number: "Sigma Z: " 1

sx = Dialog.getNumber();
sy = Dialog.getNumber();
sz = Dialog.getNumber();

open(getArgument());
run("Gaussian Blur 3D...", "x=" + sx + " y=" + sy + " z=" + sz);
```

#### radio

Add a grid of radio buttons (exclusive checkboxes). Use `Dialog.getRadioButton()` to retrieve the
label of the selected radio button.

```
//! type: file
//? radio: "Origin: " 3 3 Top-Left Top-Center Top-Right Center-Left Center Center-Right Bottom-Left Bottom-Center Bottom-Right
//? number: "Final size: " 512 0 3 px

origin = Dialog.getRadioButton();
size   = Dialog.getNumber();

open(getArgument());
run("Canvas Size...", "width=" + size + " height=" + size + " position=" + origin + " zero");
```

#### slider

Add a slider ranging from a minimum value to a maximum value. Contrary to ImageJ, the number is
always a double. The resolution of the slider is 1/1000, which is not ideal, but is most likely
not a problem on most of monitors. A custom, more precise value can be input manually in the
numeric field. Use `Dialog.getNumber()` to retrieve the value.

```
//! type: file
//? slider: Default\ Z\ position:\  1 100 1

z = round(getNumber());

open(getArgument());
z = maxOf(1, minOf(nSlices, z));
setSlice(z);
```

#### string

Add a text field to allow the user to input free text. Use `Dialog.getString()` to retrieve the
value.

```
//? message: "This macro is a badly designed calculator. Enjoy!"
//? string: "Operation: " 1+1

operation = Dialog.getString();
result    = eval("js", operation);
print(operation + " = " + result);
```
