/*
 * Property is thief.
 */
package eu.hubrecht.fiji.superdroplet;

/**
 *
 * @author d.fabreges
 */
public class Future {
    
    public static class String {
    
        public static java.lang.String strip(java.lang.String str) {
            int length = str.length();

            int il = 0;
            while (il < length && str.charAt(il) == ' ') {
                il ++;
            }

            if (il == length) {
                return "";
            }

            int ir = length - 1;
            while (ir >= 0 && str.charAt(ir) == ' ') {
                ir --;
            }

            return str.substring(il, ir+1);
        }  

        public static java.lang.String join(java.lang.String delimiter, Object[] arr) {
            java.lang.String ret = "";
            for (int i = 0; i<arr.length; i++) {
                if (i > 0)
                    ret += delimiter;
                
                ret += arr[i].toString();
            }
            
            return ret;
        }
    }
    
}