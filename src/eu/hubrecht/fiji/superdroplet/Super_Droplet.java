/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.hubrecht.fiji.superdroplet;

import eu.hubrecht.fiji.superdroplet.awtext.NumericField;
import com.wordpress.tips4java.layout.RelativeLayout;
import eu.hubrecht.fiji.superdroplet.awtext.BrowseButton;
import eu.hubrecht.fiji.superdroplet.awtext.PromptDialog;
import eu.hubrecht.fiji.superdroplet.parser.ExecutionBox;
import eu.hubrecht.fiji.superdroplet.parser.ExecutionLine;
import ij.WindowManager;
import ij.plugin.frame.PlugInFrame;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JSlider;

/**
 *
 * @author fabreges
 */
public class Super_Droplet extends PlugInFrame implements DropTargetListener, Runnable, ActionListener, ItemListener
{
    File actionPath;
    Label  dndLabel    = new Label();
    Choice dndChoice   = new Choice();
    Button dndEdit     = new Button();
    Button dndAdd      = new Button();
    Container settings = new Container();
    ExecutionBox ebox  = null;
    String checksum    = null;
    
    InMemoryPreferences preferences = new InMemoryPreferences();
    
    List<Component> components = new ArrayList<>();
    String[] labelValues = new String[]{"Drag files to process ", "Drop here!", "Running %d/%d"};
    
    BlockingQueue<Job> queue = new LinkedBlockingDeque<>();
    
    public Super_Droplet() {
        super("Super Droplet");
        
        actionPath = new File(ij.IJ.getDirectory("plugins"), "Droplet Actions");
        if (ij.IJ.versionLessThan("1.43i")) {
            ij.IJ.error("Super droplet requires ImageJ 1.43i or above.");
            return;
        }
        
        if (!actionPath.exists()) {
            actionPath.mkdirs();
            
            try (PrintWriter out = new PrintWriter(new File(actionPath, "Sequence projection.ijm"))) {
                out.println(exampleMacro());
            } catch (FileNotFoundException ex) {
                ij.IJ.error("Unable to access the Droplet Action folder");
                return;
            }
        }
        
        File[] macros = actionPath.listFiles(f -> f.getName().toLowerCase().endsWith(".ijm"));
        if (macros.length == 0) {
            ij.IJ.error("The Droplet Action folder is empty");
            return;
        }
        
        for (File macro : macros) {
            String name = macro.getName();
            name = name.substring(0, name.length() - 4);
            
            dndChoice.addItem(name);
        }
        
        new Thread(this).start();
        
        initComponents();
    }
    
    private void initComponents() {
        dndLabel.setText(labelValues[0]);
        
        dndEdit.setLabel("...");
        dndEdit.addActionListener(this);
        
        dndAdd.setLabel("+");
        dndAdd.addActionListener(this);
        
        dndChoice.addItemListener(this);
        
        settings.setLayout(new BoxLayout(settings, BoxLayout.Y_AXIS));
        
        Container macroSelection = new Container();
        macroSelection.setLayout(new FlowLayout());
        macroSelection.add(dndLabel);
        macroSelection.add(dndChoice);
        macroSelection.add(dndEdit);
        macroSelection.add(dndAdd);
        
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(macroSelection);
        this.add(settings);
        this.pack();
        
        ij.gui.GUI.center(this);
        new DropTarget(this, this);
        WindowManager.addWindow(this);
        this.setVisible(true);
        
        itemStateChanged(new ItemEvent(dndChoice, dndChoice.getSelectedIndex(), dndChoice.getSelectedItem(), ItemEvent.SELECTED));
    }

    @Override
    public void drop(DropTargetDropEvent dtde) {
        if (ebox == null) {
            ij.IJ.error("Please select a macro first");
            return;
        }
        
        dtde.acceptDrop(DnDConstants.ACTION_COPY);
        DataFlavor[] flavors;
      
        try {
            Transferable trans = dtde.getTransferable();
            flavors = trans.getTransferDataFlavors();
            
            List<File> files = new ArrayList<>();
            for (DataFlavor flavor : flavors) {
                
                if (flavor.isFlavorJavaFileListType()) {
                    files = (List) trans.getTransferData(DataFlavor.javaFileListFlavor);
                    break;
                } else if (flavor.isFlavorTextType()) {
                    Object data = trans.getTransferData(flavor);
                    if (!(data instanceof String)) continue;
                    
                    files.add(new File(data.toString().trim()));
                    
                } else {
                    //ij.IJ.error("Unknown content type: " + flavor.getMimeType());
                    //break;
                }
            }
            
            InMemoryPreferences macroPref = (checksum == null ?
                                             new InMemoryPreferences() : 
                                             preferences.child(checksum));
            
            String dialogString = "_=Dialog.create('');";
            int cno = 0;
            for (Object c : components) {
                String uname = "C" + ++cno;
                
                if (c instanceof Checkbox) {
                    Checkbox cb = (Checkbox) c;
                    if (cb.getCheckboxGroup() == null) {
                        dialogString += "\nDialog.addCheckbox('', " + (((Checkbox) c).getState() ? "true" : "false") + ");";
                        macroPref.put(uname, ((Checkbox) c).getState());
                    } else {
                        cb = cb.getCheckboxGroup().getSelectedCheckbox();
                        if (cb == null) {
                            dialogString += "\nDialog.addRadioButtonGroup('', newArray('_'), 1, 1, '');";
                        } else {
                            String rlabel = cb.getLabel().replace("'", "\\'");
                            dialogString += "\nDialog.addRadioButtonGroup('', newArray('" + rlabel + "'), 1, 1, '" + rlabel + "');";
                            macroPref.put(uname, cb.getLabel());
                        }
                    }
                } else if (c instanceof Choice) {
                    String value = ((Choice) c).getSelectedItem();
                    dialogString += "\nDialog.addChoice('', newArray('" + value.replace("'", "\\'") + "'));";
                    macroPref.put(uname, value);
                } else if (c instanceof Label) {
                    String value = ((Label) c).getText();
                    dialogString += "\nDialog.addString('', '" + value.replace("'", "\\'") + "');";
                    macroPref.put(uname, value);
                } else if (c instanceof NumericField) {
                    String value = ((NumericField) c).getText();
                    dialogString += "\nDialog.addNumber('', '" + value.replace("'", "\\'") + "');";
                    try {
                        macroPref.put(uname, Double.parseDouble(value));
                    } catch(NumberFormatException ex) {
                        macroPref.remove(uname);
                    }
                } else if (c instanceof TextField) {
                    String value = ((TextField) c).getText();
                    dialogString += "\nDialog.addString('', '" + value.replace("'", "\\'") + "');";
                    macroPref.put(uname, value);
                } 
            }
            
            HashMap<String,List<String>> filelists = buildFileLists(files);
            String[] groups = new String[filelists.size()];
            filelists.keySet().toArray(groups);
            Arrays.sort(groups);
            
            for (String group : groups) {
                Object[] filearr = filelists.get(group).toArray();
                Arrays.sort(filearr);
                
                String arguments = Future.String.join("\n", filearr);
                Job job = new Job(ebox.getMacro(), dialogString, arguments);
                queue.add(job);
            }

            dtde.dropComplete(true);
        } catch (UnsupportedFlavorException | IOException ufe) {
            dtde.dropComplete(false);
        }
    }
    
    private HashMap<String,List<String>> buildFileLists(List<File> files) {
        List<Object[]> groupOpt = ebox.getOptionValues("group");
        String group;
        if (groupOpt.isEmpty()) {
            group = "none";
        } else {
            group = (String) groupOpt.get(groupOpt.size() - 1)[0];
        }
        
        List<Object[]> recursiveOpt = ebox.getOptionValues("recursive");
        boolean recursive;
        if (recursiveOpt.isEmpty()) {
            recursive = false;
        } else {
            recursive = (boolean) recursiveOpt.get(recursiveOpt.size() - 1)[0];
        }
        
        List<Object[]> typeOpt = ebox.getOptionValues("type");
        String type;
        if (typeOpt.isEmpty()) {
            type = "both";
        } else {
            type = (String) typeOpt.get(typeOpt.size() - 1)[0];
        }
        
        List<File> allfiles = new ArrayList<>();
        List<File> fileleft = new ArrayList<>(files);
        
        for (int i = 0; i<fileleft.size(); i++) {
            File file = fileleft.get(i);
            if (!file.exists()) continue;
            
            if (recursive && file.isDirectory()) {
                fileleft.addAll(Arrays.stream(file.listFiles()).collect(Collectors.toList()));
            }
            
            allfiles.add(file);
        }
        
        if ("file".equals(type)) {
            allfiles = allfiles.stream().filter(f -> f.isFile()).collect(Collectors.toList());
        } else if ("folder".equals(type)) {
            allfiles = allfiles.stream().filter(f -> f.isDirectory()).collect(Collectors.toList());
        }
        
        HashMap<String, List<String>> ret = new HashMap<>();
        if ("none".equals(group)) {
            for (int i = 0; i<allfiles.size(); i++) {
                List<String> fpath = new ArrayList<>();
                fpath.add(allfiles.get(i).getAbsolutePath());
                ret.put("file " + i, fpath);
            }
        } else if ("folder".equals(group)) {
            for (int i = 0; i<allfiles.size(); i++) {
                File fpath   = allfiles.get(i);
                String dpath = fpath.getParentFile().getAbsolutePath();
                
                List<String> fpaths = ret.getOrDefault(dpath, new ArrayList<>());
                fpaths.add(fpath.getAbsolutePath());
                ret.put(dpath, fpaths);
            }
        } else if ("all".equals(group)) {
            ret.put("files", allfiles.stream().map(f -> f.getAbsolutePath()).collect(Collectors.toList()));
        }
        
        return ret;
    }
    
    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        dndLabel.setText(labelValues[1]);
        dtde.acceptDrag(DnDConstants.ACTION_COPY);
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        dndLabel.setText(labelValues[1]);
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        dndLabel.setText(labelValues[0]);
    }

    @Override
    public void run() {
        int total, count = 0;
        while (true) {
            try {
                Job job = queue.take();
                
                count += 1;
                total  = queue.size() + count;
                
                dndLabel.setText(String.format(labelValues[2], count, total));
                ij.IJ.runMacroFile(job.getMacro().getCanonicalPath(), job.getArgument());
                
                if (queue.isEmpty()) {
                    count = 0;
                    dndLabel.setText(labelValues[0]);
                }
                
            } catch (IOException | InterruptedException ex) {
                if (!"Macro canceled".equals(ex.getMessage())) {
                    ij.IJ.handleException(ex);
                 }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        
        if (src == dndEdit) {
            String macroname = dndChoice.getSelectedItem();
            File macro = new File(actionPath, macroname + ".ijm");
            
            ij.IJ.run("Edit...", "open=[" + macro.getAbsolutePath() + "]");
        } else if (src == dndAdd) {
            PromptDialog createMacro = new PromptDialog(this, "");
            createMacro.addStringValidation(t -> {
                if ("".equals(t))
                    return false;
                
                for (int i = 0; i<dndChoice.getItemCount(); i++) {
                    if (dndChoice.getItem(i).toLowerCase().equals(t.toLowerCase()))
                        return false;
                }
                
                return true;
            });
            
            createMacro.setTitle("Name of the new macro");
            createMacro.setModal(true);
            createMacro.setLocationRelativeTo(null);
            createMacro.setVisible(true);
            
            if (createMacro.wasCancelled())
                return;
            
            String name  = createMacro.getString();
            File   macro = new File(actionPath, name + ".ijm");
            try (PrintWriter out = new PrintWriter(macro)) {
                out.println(defaultMacro());
            } catch (FileNotFoundException ex) {
                ij.IJ.error("Unable to access the Droplet Action folder");
                return;
            }
            
            dndChoice.addItem(name);
            ij.IJ.run("Edit...", "open=[" + macro.getAbsolutePath() + "]");
        }
    }
            
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() != ItemEvent.SELECTED) {
            return;
        }
        
        String macroname = (String) e.getItem();
        File macro = new File(actionPath, macroname + ".ijm");
        ebox = new ExecutionBox(macro);
        
        InMemoryPreferences macroPref;
        try {
            byte[] data = Files.readAllBytes(Paths.get(macro.getAbsolutePath()));
            byte[] hash = MessageDigest.getInstance("MD5").digest(data);
            checksum    = new BigInteger(1, hash).toString(16);
            macroPref   = preferences.child(checksum);
        } catch (IOException | NoSuchAlgorithmException ex) {
            macroPref = new InMemoryPreferences();
            checksum  = null;
        }

        settings.removeAll();
        components.clear();
        
        Insets defaultInsets  = new Insets(5, 5, 5, 5);
        Insets checkboxInsets = new Insets(5, 20, 5, 5);
        
        int topStrut = -1;
        int botStrut = -1;
        int elineno  = 0;
        for (ExecutionLine eline : ebox.getExecutionLines()) {
            if (eline.getType() != ExecutionLine.TYPE_PARAMETERS) {
                continue;
            }
            
            String uname = "C" + ++elineno;
            
            Container pcontainer = new Container();
            Insets usedInsets = defaultInsets;
            switch (eline.getName()) {
                case "checkbox":
                {
                    pcontainer.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    Checkbox cb = new Checkbox(eline.getStringValue(0), macroPref.getBoolean(uname, eline.getBoolValue(1)));
                    pcontainer.add(Box.createHorizontalStrut(checkboxInsets.left));
                    pcontainer.add(cb, 1f);
                    pcontainer.add(Box.createHorizontalStrut(checkboxInsets.right));
                    components.add(cb);
                    
                    usedInsets = checkboxInsets;
                    break;
                }
                
                case "choice":
                {
                    pcontainer.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    Label plabel   = new Label(eline.getStringValue(0));
                    Choice pchoice = new Choice();
                    for (int i = 1; i<eline.getNumValues(); i++) {
                        pchoice.addItem(eline.getStringValue(i));
                    }
                    
                    pchoice.select(macroPref.get(uname, pchoice.getItem(0)));
                    
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.left));
                    pcontainer.add(plabel,  1f);
                    pcontainer.add(pchoice, 1f);
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.right));
                    components.add(pchoice);
                    
                    usedInsets = defaultInsets;
                    break;
                }
                
                case "file":
                {
                    pcontainer.setLayout(new BoxLayout(pcontainer, BoxLayout.Y_AXIS));
                    
                    Label plabel = new Label(eline.getStringValue(0));
                    Label ppath    = new Label(macroPref.get(uname, ""));
                    BrowseButton pbrowse = new BrowseButton(
                            (eline.getNumValues() == 1 ? "" : eline.getStringValue(1)),
                            "Browse...");
                    pbrowse.addFileSelectionListener((fse) -> ppath.setText(fse.getFilepath()));
                    
                    Container clabel = new Container();
                    clabel.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    clabel.add(Box.createHorizontalStrut(defaultInsets.left));
                    clabel.add(plabel, 1f);
                    clabel.add(Box.createHorizontalStrut(defaultInsets.right));
                    
                    Container cbrowse = new Container();
                    cbrowse.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    cbrowse.add(Box.createHorizontalStrut(defaultInsets.left));
                    cbrowse.add(ppath, 1f);
                    cbrowse.add(pbrowse);
                    cbrowse.add(Box.createHorizontalStrut(defaultInsets.right));
                    
                    pcontainer.add(clabel);
                    pcontainer.add(cbrowse);
                    components.add(ppath);
                    
                    usedInsets = defaultInsets;
                    break;
                }
                
                case "message":
                {
                    pcontainer.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    Label plabel = new Label(eline.getStringValue(0));
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.left));
                    pcontainer.add(plabel, 1f);
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.right));
                    
                    usedInsets = defaultInsets;
                    break;
                }
                
                case "number":
                {
                    pcontainer.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    int decimal = (eline.getNumValues() == 5 ? eline.getIntValue(2) : 3);
                    int columns = (eline.getNumValues() == 5 ? eline.getIntValue(3) : 0);
                    String unit = (eline.getNumValues() == 5 ? eline.getStringValue(4) : null);
                    
                    Label plabel = new Label(eline.getStringValue(0));
                    NumericField pnum = new NumericField(macroPref.getDouble(uname, eline.getDblValue(1)), decimal);
                    if (columns > 0) pnum.setColumns(columns);
                    
                    Container clabel = new Container();
                    clabel.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    clabel.add(plabel, 1f);
                    
                    Container cfield = new Container();
                    cfield.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    if (columns == 0) cfield.add(pnum, 1f);
                    else              cfield.add(pnum);
                    
                    if (unit != null) {
                        cfield.add(new Label(unit));
                    }
                    
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.left));
                    pcontainer.add(clabel, 1f);
                    pcontainer.add(cfield, 1f);
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.right));
                    components.add(pnum);
                    
                    usedInsets = defaultInsets;
                    break;
                }
                
                case "radio":
                {
                    pcontainer.setLayout(new GridLayout(2, 1));
                    
                    int rows = eline.getIntValue(1);
                    int cols = eline.getIntValue(2);
                    
                    Label plabel = new Label(eline.getStringValue(0));
                    CheckboxGroup pradio = new CheckboxGroup();
                    
                    Container clabel = new Container();
                    clabel.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    clabel.add(Box.createHorizontalStrut(defaultInsets.left));
                    clabel.add(plabel, 1f);
                    clabel.add(Box.createHorizontalStrut(defaultInsets.right));
                    
                    Container ccradio = new Container();
                    ccradio.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    Container cradio = new Container();
                    cradio.setLayout(new GridLayout(rows, cols, 5, 3));
                    String selected = macroPref.get(uname, null);
                    for (int i = 3; i<eline.getNumValues(); i++) {
                        String label = eline.getStringValue(i);
                        boolean sel  = (selected == null && i==3) || (selected != null && selected.equals(label));
                        Checkbox cb = new Checkbox(label, sel, pradio);
                        cradio.add(cb);
                        
                        if (i == 3) {
                            components.add(cb);
                        }
                    }
                    
                    ccradio.add(Box.createHorizontalStrut(checkboxInsets.left));
                    ccradio.add(cradio, 1f);
                    ccradio.add(Box.createHorizontalStrut(checkboxInsets.right));
                    
                    pcontainer.add(clabel);
                    pcontainer.add(ccradio);
                    
                    usedInsets = checkboxInsets;
                    break;
                }
                
                case "slider":
                {
                    pcontainer.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    double min = eline.getDblValue(1);
                    double max = eline.getDblValue(2);
                    double def = macroPref.getDouble(uname, eline.getDblValue(3));
                    
                    if (min > max) {
                        double tmp = min;
                        min = max;
                        max = tmp;
                    }
                    
                    if (def < min || def > max) {
                        def = (min + max) / 2;
                    }
                    
                    int fval = 0;
                    if (min < max) {
                        fval = (int) Math.round(1000 * (def - min) / (max - min));
                    }
                    
                    Label plabel = new Label(eline.getStringValue(0));
                    JSlider pslider = new JSlider(0, 1000, fval);
                    NumericField pnum = new NumericField(def, 3);
                    pnum.setColumns(5);
                    
                    double _min = min, _max = max;
                    pslider.addChangeListener(ce -> {
                        double tval = _min + (_max - _min) * pslider.getValue() / 1000.0;
                        pnum.setText("" + tval);
                    } );
                    
                    pnum.addTextListener(te -> {
                        try {
                            double number = Double.parseDouble(pnum.getText());
                            int _fval = 0;
                            if (_min < _max) {
                                _fval = (int) Math.round(1000 * (number - _min) / (_max - _min));
                            }
                            _fval = Math.round(_fval * 1000) / 1000;
                            pslider.setValue(_fval);
                        } catch(NumberFormatException nfe) { }
                    });
                    
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.left));
                    pcontainer.add(plabel);
                    pcontainer.add(pslider, 1f);
                    pcontainer.add(pnum);
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.right));
                    components.add(pnum);
                    
                    usedInsets = defaultInsets;
                    break;
                }
                
                case "string":
                {
                    pcontainer.setLayout(new RelativeLayout(RelativeLayout.X_AXIS));
                    
                    Label plabel   = new Label(eline.getStringValue(0));
                    TextField pstring = new TextField(macroPref.get(uname, eline.getStringValue(1)));
                    
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.left));
                    pcontainer.add(plabel,  1f);
                    pcontainer.add(pstring, 1f);
                    pcontainer.add(Box.createHorizontalStrut(defaultInsets.right));
                    components.add(pstring);
                    
                    usedInsets = defaultInsets;
                    break;
                }
            }
            
            if (botStrut >= 0) { topStrut = Math.max(botStrut, usedInsets.top); }
            botStrut = usedInsets.bottom;

            if (topStrut > 0) { settings.add(Box.createVerticalStrut(topStrut)); }
            settings.add(pcontainer);
        }
        
        settings.add(Box.createVerticalStrut(10));
        
        this.pack();
    }
    
    @Override public void dropActionChanged(DropTargetDragEvent dtde) { }
    
    
    private String exampleMacro() {
        String content = "";
        content += "//! group: folder\n";
        content += "//! type: file\n";
        content += "//! recursive: true\n";
        content += "//? checkbox: 'Set batch mode' true\n";
        content += "//? choice: 'Projection axis: ' 'X' 'Y' 'Z'\n";
        content += "//? choice: 'Projection type: ' 'Average Intensity' 'Max Intensity' 'Min Intensity' 'Sum Slices' 'Standard Deviation' 'Median'\n\n";
        content += "files = split(getArgument(), \"\\n\");\n";
        content += "Array.sort(files);\n\n";
        content += "if (Dialog.getCheckbox()) {\n";
        content += "    setBatchMode(true);\n";
        content += "}\n\n";
        content += "axis = Dialog.getChoice();\n";
        content += "type = Dialog.getChoice();\n\n";
        content += "finalName = \"final\";\n";
        content += "for (f = 0; f<lengthOf(files); f++) {\n";
        content += "    open(files[f]);\n";
        content += "    refimg = getImageID();\n";
        content += "    if (axis == \"X\") {\n";
        content += "        run(\"Reslice [/]...\", \"output=1.000 start=Left\");\n";
        content += "        selectImage(refimg); close();\n";
        content += "        refimg = getImageID();\n";
        content += "    } else if (axis == \"Y\") {\n";
        content += "        run(\"Reslice [/]...\", \"output=1.000 start=Top\");\n";
        content += "        selectImage(refimg); close();\n";
        content += "        refimg = getImageID();\n";
        content += "    }\n\n";
        content += "    run(\"Z Project...\", \"projection=[\" + type + \"]\");\n";
        content += "    selectImage(refimg); close();\n\n";
        content += "    if (f == 0) {\n";
        content += "        finalName = getTitle();\n";
        content += "    } else {\n";
        content += "        run(\"Concatenate...\", \"open title=[\" + finalName + \"] image1=[\" + finalName + \"] image2=[\" + getTitle() + \"]\");\n";
        content += "    }\n";
        content += "}\n\n";
        content += "setBatchMode(\"exit & display\");";

        return content;
    }
    
    private String defaultMacro() {
        String content = "";
        content += "//  Group the files individually (none), by folder (folder) or all together (all)\n";
        content += "//! group: none\n\n";
        content += "//  Accept only files (file), folders (folder), or both (both)\n";
        content += "//! type: both\n\n";
        content += "//  If folders are dropped, explore the content recursively (true) or not (false)\n";
        content += "//! recursive: false\n\n";
        content += "//? checkbox: 'This is a checkbox' true\n";
        content += "//? choice: 'This is the question: ' 'To be' 'Not to be'\n";
        content += "//? file: 'Hamlet epub: '\n";
        content += "//? message: 'Short interval/intermission'\n";
        content += "//? number: 'Try to read ' 314 0 3 pages\n";
        content += "//? radio: 'Handedness' 1 3 Left Ambi Right\n";
        content += "//? slider: 'Age' 0 130 30\n";
        content += "//? string: 'Tell me your name: ' Link\n";

        return content;
    }
    
}








